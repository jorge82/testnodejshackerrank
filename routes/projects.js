var data = require("../data-store");
var projects = data.getProjects();
var router = require("express").Router();

function getDictionary(data) {
  let dict = {};
  for (let i in data) {
    dict[data[i].id] = data[i];
  }
  return dict;
}

router.get("/", (req, res, next) => {
  let projectsResponse = projects;
  if (projectsResponse.length == 0) {
    res.status(200).json(projectsResponse);
  } else {
    projectsResponse.sort((a, b) => a.id - b.id);
    res.status(200).json(projectsResponse);
  }
});
router.get("/:id", (req, res, next) => {
  let id = req.params.id;

  if (id == "active") {
    let projectsResponse = projects.filter((p) => p.isActive);
    res.status(200).json(projectsResponse);
  } else {
    const projectDictionary = getDictionary(projects);
    let project = projectDictionary[id];
    if (project) {
      res.status(200).json(project);
    } else {
      res.status(404).json({ message: "No Project Found" });
    }
  }
});

module.exports = router;
